# JS Simple ModalYR



## Как пользоваться

Скачиваем файл и подключам на проект ModalYR.js

В месте где нужно вызвать модальное окно прописываем 
```
let title = 'Название модального окна';
let body = 'содержимое модального окна';

ModalYR.open(title,body);

```
![img/img.png](img/img.png)
