document.addEventListener('DOMContentLoaded', function () {
    class ModalYR {
        instance = null

        constructor() {
            this.setEvents();
        }

        static  setInstance(){
            if (!this.instance) this.instance = new ModalYR();
        }

        createModal(){
            let modal_YR = document.createElement("div");
            modal_YR.id = "modal-YR-wrap";
            modal_YR.className = "modal-YR";
            modal_YR.role = "dialog";
            modal_YR.tabIndex = "-1";
            modal_YR.innerHTML = `
                <div class="modal-YR--inner">
                    <div class="modal-YR--header">
                        <h3 class="modal-YR--title">YR Modal title</h3>
                        <svg fill="currentColor" class="modal-YR--close" version="1.1"
                             xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 32 32">
                            <path d="M25.313 8.563l-7.438 7.438 7.438 7.438-1.875 1.875-7.438-7.438-7.438 7.438-1.875-1.875 7.438-7.438-7.438-7.438 1.875-1.875 7.438 7.438 7.438-7.438z"></path>
                        </svg>
                    </div>
                    <p class="modal-YR--body">YR Modal body</p>
                </div>`;
            document.body.appendChild(modal_YR);
            this.modal_YR = modal_YR;
            this.setStyle();
        }

        setOverlay(){
            let overlay = document.createElement("div");
            overlay.className = "modal-YR--overlay";
            overlay.id = "modal-YR--overlay";
            document.body.appendChild(overlay);
        }

        static open(title="YR Modal title",body="YR Modal body"){
            this.setInstance();
            this.instance.createModal();
            this.instance.setParamsText(title,body);


            document.body.style.overflow = "hidden";
            this.instance.modal_YR.setAttribute("open", "true");
            document.addEventListener("keydown", this.instance.escClose);
            this.instance.setOverlay();
        };

        closeModal(){
            document.body.style.overflow = "auto";
            this.modal_YR.removeAttribute("open");
            document.removeEventListener("keydown", this.escClose);
            document.body.removeChild(document.getElementById("modal-YR--overlay"));
            document.body.removeChild(document.getElementById("modal-YR-wrap"));
        }
        escClose(e){
            if (e.keyCode == 27) {
                this.closeModal();
            }
        };
        setParamsText(title,body){
            this.modal_YR.querySelector(".modal-YR--title").innerText = title;
            this.modal_YR.querySelector(".modal-YR--body").innerHTML = body;
        }

        setEvents(){
            document.addEventListener("click", (e) => {
                let className = e.target.className;
                if(e.target.tagName=="svg")className = className.baseVal;

                if (className==="modal-YR-open") this.open();
                else if (className === "modal-YR--close" || className === "modal-YR--overlay" || className==="modal-YR") this.closeModal();
                else return;
            });
        }

        setStyle(){
            let style = document.createElement("style");
            style.innerHTML = `
                   .modal-YR {
                        -webkit-transition:opacity 0.2s;
                        -moz-transition:opacity 0.2s;
                        transition:opacity 0.2s;
                        opacity: 0;
                        align-items: center;
                        justify-content: center;
                        z-index: 0;
                        width: 100%;
                        position: absolute;
                        left: 0;
                        top: 30vh;
                    }

                    .modal-YR[open] {
                        opacity: 1;
                        z-index: 1000;
                    }

                    .modal-YR--inner {
                        background-color: white;
                        border-radius: 0.5em;
                        width: 600px;
                        padding: 2em;
                        margin: auto;
                    }

                    @media screen and (max-width: 670px) {
                        .modal-YR--inner{
                            width: 80%;
                        }
                    }

                    .modal-YR--header {
                        display: flex;
                        align-items: center;
                        justify-content: space-between;
                        border-bottom: 2px solid black;
                    }

                    .modal-YR--header h3 {
                        margin: unset;
                    }
                    .modal-YR--header{
                        padding-bottom: 10px;
                    }

                    #modal-YR--overlay {
                        width: 100%;
                        height: 100%;
                        position: fixed;
                        top: 0;
                        left: 0;
                        z-index: 999;
                        background-color: #191919;
                        opacity: 0.5;
                    }

                    .modal-YR--close {
                        font-size: 1.4rem;
                        color: #191919;
                        cursor: pointer;
                    }
                `;
            this.modal_YR.appendChild(style);
        }
    }
    window.ModalYR = ModalYR;
});